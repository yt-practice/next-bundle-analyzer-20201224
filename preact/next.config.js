const path = require('path')
const basePath = ''
const withBundleAnalyzer = require('@next/bundle-analyzer')({
	enabled: process.env.ANALYZE === 'true',
})
module.exports = withBundleAnalyzer({
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	webpack(config, _options) {
		config.resolve.alias = {
			...config.resolve.alias,
			'~': path.resolve(__dirname, 'src'),
		}
		// config.node = {
		// 	...config.node,
		// 	fs: 'empty',
		// 	child_process: 'empty',
		// }
		return config
	},
})
