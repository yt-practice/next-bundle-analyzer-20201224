

https://zenn.dev/rithmety/articles/20201224-nextjs-on-preact

# react

`yarn add next react react-dom @types/node @types/react typescript @next/bundle-analyzer`

`yarn start` で `localhost:3002` に立つ

# preact

`yarn add next preact preact-render-to-string github:preact-compat/react#1.0.0 github:preact-compat/react-dom#1.0.0 @types/node @types/react typescript @next/bundle-analyzer`

`yarn start` で `localhost:3001` に立つ
